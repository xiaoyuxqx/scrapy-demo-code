import json
import csv
from pprint import pprint

with open('weather.json') as data_file:
	datas = json.load(data_file)

# remove duplicate
#datas = { each['biz_id'] : each for each in datas_ds }.values()

# for debug, close to speed up
#pprint(datas)

# CSV fromat
# Buz_id, Week 
f = csv.writer(open("weather.csv", "wb+"))

# Write CSV Header
# mysql foreign key dosen't like header
#f.writerow(["zipCode", "date", "temperature", "precipitation", "dewPoint")

for data in datas:
	f.writerow([
		data["zipCode"],
		data["date"],
		float(data["temperature"][0]),
		float(data["precipitation"][0]),
		float(data['dewPoint'][0])
	])

import scrapy
import datetime

from weather.items import WeatherItem

# example weather information website
# http://www.almanac.com/weather/history/zipcode/63130/2015-7-20

class  WeatherSpider(scrapy.Spider):
	# unique name for scrapy
	name = "weather"
	# the only domain allow to traverse
	allow_domains = ["almanac.com/"]
	zipCodes = ['63130', '63105']
	# zipCodes = [
	# 	'63101', '63102', '63103', '63104', '63105', '63106', '63107', '63108', '63109', '63110', '63111', '63112', '63113', '63114', '63115', '63116', '63117', '63118', 
	# 	'63119', '63120', '63121', '63122', '63123', '63124', '63125', '63126', '63127', '63128', '63129', '63130', '63131', '63132', '63133', '63134', '63135', '63136', 
	# 	'63137', '63138', '63139', '63140', '63141', '63142', '63143', '63144', '63145', '63146', '63147', '63148', '63149', '63150', '63151', '63152', '63153', '63154', 
	# 	'63155', '63156', '63157', '63158', '63159', '63160', '63161', '63162', '63163', '63164', '63165', '63166', '63167', '63168', '63169', '63170', '63171', '63172', 
	# 	'63173', '63174', '63175', '63176', '63177', '63178', '63179', '63180', '63181', '63182', '63183', '63184', '63185', '63186', '63187', '63188', '63189', '63190', 
	# 	'63191', '63192', '63193', '63194', '63195', '63196', '63197', '63198', '63199']
	
	dates = []
	for year in range(2015, 2016):
		for month in range(7, 8):
			for day in range(1,21):
				try:
					dt = datetime.date(year, month, day)
					dates.append( "%s" % (dt))
				except:
					pass # ignore out of bound excetption
				
	start_urls = []
	for z in zipCodes:
		for d in dates:
			start_urls.append( "http://www.almanac.com/weather/history/zipcode/%s/%s" % (z, d) )

	def parse(self, response):
		item = WeatherItem()	
		item['zipCode'] = response.url.split('/')[-2]
		item['date'] = response.url.split('/')[-1]
		item['temperature'] = response.xpath('//div[@class="weatherhistory_results_datavalue temp"]/p/span[@class="value"]/text()').extract()
		item['precipitation'] = response.xpath('//div[@class="weatherhistory_results_datavalue prcp"]/p/span[@class="value"]/text()').extract()
		item['dewPoint'] = response.xpath('//div[@class="weatherhistory_results_datavalue dewp"]/p/span[@class="value"]/text()').extract()
		if not item['temperature']:
			return
		yield item
		
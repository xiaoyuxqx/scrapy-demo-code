import json
import csv
from pprint import pprint

with open('biz.json') as data_file:
	datas_ds = json.load(data_file)

# remove duplicate
datas = { each['biz_id'] : each for each in datas_ds }.values()

# for debug, close to speed up
#pprint(datas)

# CSV fromat
# Buz_id, Week 
f = csv.writer(open("hours.csv", "wb+"))

# Write CSV Header
# mysql foreign key dosen't like header
#f.writerow(["Biz_id", "Weekday", "Open_time", "Close_time")

for data in datas:
	for weekday in data["hours"].keys():
		f.writerow([
			data["biz_id"],
			weekday,
			data["hours"][weekday][0] if data["hours"][weekday] else None,
			data["hours"][weekday][1] if data["hours"][weekday] else None
		])

import json
import csv
from pprint import pprint

with open('biz.json') as data_file:
	datas_ds = json.load(data_file)

# remove duplicate
datas = { each['biz_id'] : each for each in datas_ds }.values()

# for debug, close to speed up
#pprint(datas)

# CSV fromat
# Buz_id, Week 
f = csv.writer(open("service.csv", "wb+"))

# Write CSV Header
# mysql foreign key dosen't like header
#f.writerow(["Biz_id", "Noise_level", "Has_wifi", "Has_takeout", "Has_TV", "Has_parking", 
#	"Good_for_kid", "Good_for_breakfast", "Good_for_lunch", "Good_for_dinner", "Good_for_latenight")

for data in datas:
	service = data["service"]
	s_key = service.keys()
	f.writerow([
		data["biz_id"],
		service["Noise Level"] if service["Noise Level"] else None,
		service["Wi-Fi"] == "Free" if data["service"]["Wi-Fi"] else None,
		service["Take-out"] == "Yes" if service["Take-out"] else None,
		service["Has TV"] == "Yes" if service["Has TV"] else None,
		len(service["Parking"]) != 0 if service["Parking"] else None,
		service["Good for Kids"] == "Yes" if "Good for Kids" in s_key and service["Good for Kids"] else None,
		"Breakfast" in service["Good For"],
		"Lunch" in service["Good For"],
		"Dinner" in service["Good For"],
		"Latenight" in service["Good For"]
	])

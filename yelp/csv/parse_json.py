import json
import csv
from pprint import pprint

with open('biz.json') as data_file:
	datas_ds = json.load(data_file)

# remove duplicate
datas = { each['biz_id'] : each for each in datas_ds }.values()

# for debug, close to speed up
#pprint(datas)

# CSV fromat
# Biz_id, Name, Price_range, Reviews_count, Phone_no, Avg_star_rating, Street_address, Address_locality, Address_region, Zip_code
f = csv.writer(open("biz.csv", "wb+"))

# Write CSV Header
# mysql foreign key dosen't like header
#f.writerow(["Biz_id", "Name", "Price_range", "Reviews_count", "Phone_no", "Avg_star_rating", "Street_address", "Address_locality", "Address_region", "Zip_code"])

for data in datas:
	f.writerow([
		data["biz_id"],
		data["name"],
		data["priceRange"].count('$') if data["priceRange"] else None,
		data["reviewCount"],
		data["telephone"],
		data["starRating"],
		data["address"]["streetAddress"],
		data["address"]["addressLocality"],
		data["address"]["addressRegion"],
		data["address"]["postalCode"]
	])

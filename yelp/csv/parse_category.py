import json
import csv
from pprint import pprint

with open('biz.json') as data_file:
	datas_ds = json.load(data_file)

# remove duplicate
#datas = { each['biz_id'] : each for each in datas_ds }.values()

# for debug, close to speed up
#pprint(datas)

# CSV fromat
# Buz_id, Week 
f = csv.writer(open("category.csv", "wb+"))

# Write CSV Header
# mysql foreign key dosen't like header
#f.writerow(["Biz_id", "Noise_level", "Has_wifi", "Has_takeout", "Has_TV", "Has_parking", 
#	"Good_for_kid", "Good_for_breakfast", "Good_for_lunch", "Good_for_dinner", "Good_for_latenight")

for data in datas_ds:
	for item in set(data['category']):
		f.writerow([
			data["biz_id"],
			item	
		])

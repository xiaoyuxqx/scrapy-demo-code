import scrapy
import re

from yelp.items import YelpItem
from yelp.items import AddressItem
from yelp.items import ServiceItem
from yelp.items import HoursItem

class YelpSpider(scrapy.Spider):
	name = "yelp"
	allow_domains = ["yelp.com"]
	zipCodes = ['63130']
	# zipCodes = [
	# 	'63101', '63102', '63103', '63104', '63105', '63106', '63107', '63108', '63109', '63110', '63111', '63112', '63113', '63114', '63115', '63116', '63117', '63118', 
	# 	'63119', '63120', '63121', '63122', '63123', '63124', '63125', '63126', '63127', '63128', '63129', '63130', '63131', '63132', '63133', '63134', '63135', '63136', 
	# 	'63137', '63138', '63139', '63140', '63141', '63142', '63143', '63144', '63145', '63146', '63147', '63148', '63149', '63150', '63151', '63152', '63153', '63154', 
	# 	'63155', '63156', '63157', '63158', '63159', '63160', '63161', '63162', '63163', '63164', '63165', '63166', '63167', '63168', '63169', '63170', '63171', '63172', 
	# 	'63173', '63174', '63175', '63176', '63177', '63178', '63179', '63180', '63181', '63182', '63183', '63184', '63185', '63186', '63187', '63188', '63189', '63190', 
	# 	'63191', '63192', '63193', '63194', '63195', '63196', '63197', '63198', '63199']
	start_urls = [
		"http://www.yelp.com/search?find_desc=Restaurants&find_loc=%s" % (z) for z in zipCodes 
	]

	def parse(self, response):
		for href in response.xpath('//a[@class="biz-name"]/@href'):
			url = response.urljoin(href.extract())
			yield scrapy.Request(url, callback=self.parse_biz)

		# toggle this for all pages in a query
		# next_page = response.xpath('//a[@class="page-option prev-next next"]/@href')
		# if next_page:
		# 	url = response.urljoin(next_page[0].extract())
		# 	yield scrapy.Request(url, self.parse)

	def parse_biz(self, response):
		item = YelpItem()
		item['biz_id'] = self.match_reg(response.url.split('/')[-1], '^([a-zA-Z0-9]*-[a-zA-Z0-9]*)+$')
		if not item['biz_id']: # discard empty result
			return
		item['name'] = self.first(self.trim_strs(response.xpath('//h1[@itemprop="name"]/text()').extract()))
		item['reviewCount'] = self.first(response.xpath('//span[@itemprop="reviewCount"]/text()').extract(), default=-1)
		item['telephone'] = self.first(self.trim_strs( response.xpath('//span[@itemprop="telephone"]/text()').extract() ))
		item['priceRange'] = self.first(response.xpath('//span[@itemprop="priceRange"]/text()').extract())
		item['category'] = response.xpath('//span[@class="category-str-list"]/a/text()').extract()
		item['starRating'] = self.first(response.xpath('//div[@class="biz-main-info embossed-text-white"]//meta[@itemprop="ratingValue"]/@content').extract(), default=-1)
		item['address'] = self.parse_address(response)
		item['service'] = self.parse_service(response)
		item['hours'] = self.parse_hours(response)
		yield item


	def parse_address(self, response):
		address = AddressItem()
		address.streetAddress = self.first(response.xpath('//span[@itemprop="streetAddress"]/text()').extract())
		address.addressLocality = self.first(response.xpath('//span[@itemprop="addressLocality"]/text()').extract())
		address.addressRegion = self.first(response.xpath('//span[@itemprop="addressRegion"]/text()').extract())
		address.postalCode = self.first(response.xpath('//span[@itemprop="postalCode"]/text()').extract())
		return address.toDict()

	# parse service
	def parse_service(self, response):
		service = ServiceItem()
		attributes = self.trim_strs(response.xpath('//ul[@class="ylist"]/li/div[@class="short-def-list"]/dl/dt[@class="attribute-key"]/text()').extract())
		values = self.trim_strs(response.xpath('//ul[@class="ylist"]/li/div[@class="short-def-list"]/dl/dd/text()').extract())
		for attribute, value in zip(attributes, values):
			if attribute == 'Parking': # save as array
				service.dict[attribute] = [v.strip() for v in value.split(',')]
			else:
				service.dict[attribute] = value if value else None
		return service.getDict()

	# parse hours
	def parse_hours(self, response):
		hours = HoursItem()
		wdays = response.xpath('//th[@scope="row"]/text()').extract()
		periods = response.xpath('//span[@class="nowrap"]/text()').extract() # every two related to one wdays
		if not wdays: # no info
			return hours.getDict()
		for idx in range(len(wdays)):
			hours.getDict()[wdays[idx]] = ( periods[idx*2] , periods[idx*2+1] )
		return hours.getDict() 


	# some helper function
	def match_reg(self, strings, regex):
		pattern = re.compile(regex)

		if not isinstance(strings, list):
			if pattern.match(strings) is not None:
				return pattern.match(strings).group(0)
			else:
				return []
		else:
			str_list = []
			[ str_list.append(pattern.match(s).group(0)) for s in strings]
			return str_list

	def trim_strs(self, strs):
		return [re.sub(u"(\u2018|\u2019)", "'", " ".join(s.split())) for s in strs]

	def first(self, elts, default=None):
		if elts:
			return elts[0]
		else:
			return default
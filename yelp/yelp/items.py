# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class YelpItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    biz_id = scrapy.Field()
    name = scrapy.Field()
    reviewCount = scrapy.Field(serializer=int)
    telephone = scrapy.Field()
    priceRange = scrapy.Field()
    category = scrapy.Field()
    starRating = scrapy.Field(serializer=float)
    address = scrapy.Field()
    service = scrapy.Field()
    hours = scrapy.Field()

# address
# two way to get data, I prefer the one in ServiceItem()
class AddressItem():
	streetAddress = scrapy.Field()
	addressLocality = scrapy.Field() 
	addressRegion = scrapy.Field()
	postalCode = scrapy.Field()

	def toDict(self):
		return { 
		'streetAddress': self.streetAddress,
		'addressLocality': self.addressLocality,
		'addressRegion': self.addressRegion,
		'postalCode': self.postalCode 
		}

# service
class ServiceItem():
	keys = ['Takes Reservations', 'Delivery' , 'Take-out', 'Accepts Credit Cards', 'Good For', 'Parking', 'Bike Parking', 'Wheelchair Accessible',
	'Good for Kids', 'Good for Groups', 'Attire', 'Ambience', 'Noise Level', 'Alcohol', 'Outdoor Seating', 'Wi-Fi', 'Has TV', 'Waiter Service', 'Dogs Allowed', 'Caters']
	dict = {}

	def __init__(self):
		self.dict = { key: scrapy.Field() for key in self.keys }

	def getDict(self):
		return self.dict

class HoursItem():
	keys = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
	dict = {}

	def __init__(self):
		self.dict = { key : scrapy.Field() for key in self.keys }

	def getDict(self):
		return self.dict